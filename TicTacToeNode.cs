using System;

namespace AlgoDat 
{
    public class TicTacToeNode : IComparable<TicTacToeNode>
    {
        public char[,] array2d = new char[3, 3] { { 'E', 'E', 'E' }, { 'E', 'E', 'E' }, { 'E', 'E', 'E' } };

        public override string ToString()
        {
            return CreateWordFromArray(array2d);
        }
        public int CompareTo(TicTacToeNode other)
        {
            return CreateWordFromArray(array2d).CompareTo(CreateWordFromArray(other.array2d));
        }

        public override int GetHashCode()
        {
            return CreateWordFromArray(array2d).GetHashCode();
        }

        private string CreateWordFromArray(char[,] _array)
        {
            string word = "";
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    word += _array[x, y];
                }
            }
            return word;
        }

    }
    public class TicTacToeGraph<T> where T : TicTacToeNode
    {
        public Graph<T> graph = new Graph<T>();
        public int counterEdges = 0;
        public int countAbbruch = 0;
        public Graph<T> Generate()
        {
            TicTacToeNode emptyBoard = new TicTacToeNode();
            CreateTicTacToeNode(emptyBoard, 'X');
            return graph;
        }
        public void CreateTicTacToeNode(TicTacToeNode _node, char currentSymbol)
        {
            if (CheckWin(_node.array2d))
            {
                return;
            }

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (_node.array2d[x, y] == 'E')
                    {
                        TicTacToeNode newNode = new();
                        Array.ConstrainedCopy(_node.array2d, 0, newNode.array2d, 0, _node.array2d.Length);
                        newNode.array2d[x, y] = currentSymbol;
                        graph.AddEdge((T)_node, (T)newNode);
                        CreateTicTacToeNode(newNode, SwitchSymbol(currentSymbol));
                    }
                }
            }
        }

        char SwitchSymbol(char _currentSymbol)
        {
            return _currentSymbol == 'X' ? 'O' : 'X';
        }
        public bool NoMoreEmptys(char[,] array)
        {
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (array[x, y] == 'E')
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public bool CheckWin(char[,] array)
        {
            if (array[0, 0] == 'X' && array[0, 1] == 'X' && array[0, 2] == 'X') return true;
            if (array[1, 0] == 'X' && array[1, 1] == 'X' && array[1, 2] == 'X') return true;
            if (array[2, 0] == 'X' && array[2, 1] == 'X' && array[2, 2] == 'X') return true;

            if (array[0, 0] == 'X' && array[1, 0] == 'X' && array[2, 0] == 'X') return true;
            if (array[0, 1] == 'X' && array[1, 1] == 'X' && array[2, 1] == 'X') return true;
            if (array[0, 2] == 'X' && array[1, 2] == 'X' && array[2, 2] == 'X') return true;


            if (array[0, 0] == 'O' && array[0, 1] == 'O' && array[0, 2] == 'O') return true;
            if (array[1, 0] == 'O' && array[1, 1] == 'O' && array[1, 2] == 'O') return true;
            if (array[2, 0] == 'O' && array[2, 1] == 'O' && array[2, 2] == 'O') return true;

            if (array[0, 0] == 'O' && array[1, 0] == 'O' && array[2, 0] == 'O') return true;
            if (array[0, 1] == 'O' && array[1, 1] == 'O' && array[2, 1] == 'O') return true;
            if (array[0, 2] == 'O' && array[1, 2] == 'O' && array[2, 2] == 'O') return true;


            if (array[0, 0] == 'X' && array[1, 1] == 'X' && array[2, 2] == 'X') { return true; }
            if (array[0, 2] == 'X' && array[1, 1] == 'X' && array[2, 0] == 'X') { return true; }

            if (array[0, 0] == 'O' && array[1, 1] == 'O' && array[2, 2] == 'O') { return true; }
            if (array[0, 2] == 'O' && array[1, 1] == 'O' && array[2, 0] == 'O') { return true; }

            return false;
        }

    }
}