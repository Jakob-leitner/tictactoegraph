﻿using System;

namespace AlgoDat
{
    class Program
    {
        static void Main(string[] args)
        {
           TicTacToeGraph<TicTacToeNode> tictacgraph = new TicTacToeGraph<TicTacToeNode>();
           tictacgraph.Generate();

           System.Console.WriteLine(tictacgraph.graph.nodeCounter + " = Amount of Nodes");
           System.Console.WriteLine(tictacgraph.graph);

        }
    }
}
